class User {
    _id;
    name;
    surname;
    nick;
    email;
    password;
    role;
    image;
}

export default User;