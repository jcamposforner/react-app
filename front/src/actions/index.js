import axios from 'axios';

const apiUrl = 'http://localhost:3800/api/';

export const fetchCategories = () => {
    return function (dispatch){
        return axios.get(`${apiUrl}categories`).then(response =>{
            dispatch({
                type: "FETCH_CATEGORIES",
                payload: response.data.category,
            })
        })
    }
};

export const userLogged = (user,token) => {
    return {
        type: "USER_LOGGED",
        payload: user,
        token: token,
    }
}

export const userNotLogged = (user,token) => {
    return {
        type: "USER_NOT_LOGGED",
        payload: user,
        token: token,
    }
}

export const addToCart = (product) => {
    return {
        type: "ADD_TO_CART",
        payload: product,
    }
}
export const ShippingAddressAction = (ShippingAddress) => {
    return {
        type: "SHIPPING_ADDRESS",
        payload: ShippingAddress
    }
}

export const PaymentMethodAction = (Payment) => {
    return {
        type: "PAYMENT_METHOD",
        payload: Payment,
    }
}