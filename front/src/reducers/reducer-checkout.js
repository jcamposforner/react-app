export default function (state=null, action) {
    switch (action.type) {
        case "SHIPPING_ADDRESS":
            localStorage.setItem('ShippingAddress', JSON.stringify(action.payload));
            return {
                ...state,
                ShippingAddress:action.payload,
            };
            break;
        case "PAYMENT_METHOD":
            localStorage.setItem('PaymentMethod', JSON.stringify(action.payload));
            return {
                ...state,
                PaymentMethod:action.payload,
            };
            break;

    }
    return state;
}