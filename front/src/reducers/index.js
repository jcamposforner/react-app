import {combineReducers} from 'redux';
import UserReducer from './reducer-users';
import CategoryReducer from './reducer-categories';
import UserLoggedReducer from './reducer-user-logged';
import CartReducer from './reducer-cart';
import ShippingReducer from './reducer-checkout';


const allReducers = combineReducers({
    users: UserReducer,
    categories: CategoryReducer,
    userLogged: UserLoggedReducer,
    cartReduc: CartReducer,
    Shipping: ShippingReducer
});

export default allReducers;