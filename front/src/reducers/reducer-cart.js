var initialCartState = {
    arr:[],
}

export default function (state=initialCartState, action) {
    switch (action.type) {
        case "ADD_TO_CART":
            localStorage.setItem('quantity',[...state.arr, action.payload].length);
            localStorage.setItem('cart', JSON.stringify([...state.arr, action.payload]));
            return {
                ...state,
                arr: [...state.arr, action.payload],
                mode: 'ADD',
            };
            break;

    }
    return state;
}