import React,{Component} from 'react';
import axios from "axios";
import User from "../models/user";
import {Link} from "react-router-dom";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {userLogged, userNotLogged} from "../actions";


class Login extends Component {
    constructor(props){
        super(props)

        this.state = {
            isLoading: false,
            email: '',
            password: '',
            token: localStorage.getItem('jwtToken'),
            identity: JSON.parse(localStorage.getItem('identity')),
            gettoken: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.logout = this.logout.bind(this);
    }

    handleChange(event) {
        this.setState({
            email: event.target.form.email.value,
            password: event.target.form.password.value,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ isLoading: true });

        axios.post('http://localhost:3800/api/login',this.state).then(
            (res) => {
                localStorage.setItem('identity',JSON.stringify(res.data.user));
                this.setState({
                    identity: res.data.user,
                    gettoken: true,
                });
                this.getToken();

            },
            (err) => console.log('Error autenticación')
        );

    }

    getToken(){
        axios.post('http://localhost:3800/api/login',this.state).then(
            (res) =>{
                localStorage.setItem('jwtToken',res.data.token);
                this.setState({
                    token: JSON.stringify(res.data.token),
                    gettoken: false
                });

                this.props.userLogged(this.state.identity,this.state.token);
            }
        )
    }
    logout(event){
        event.preventDefault();
        this.props.userNotLogged();
        this.setState({
            token:null,
            identity: '',
        });
        localStorage.setItem('jwtToken',null);
        localStorage.setItem('identity',null);
    }




    render() {
    console.log(this.props)
        return (
                <div id="login">
                    <div className="login-card">
                        { this.state.identity ?
                            <p>Bienvenido, {this.state.identity.nick}</p>
                                :null
                        }
                        <div className="card-title">
                            <h1>Iniciar Sesión</h1>
                            <p onClick={this.logout}>C</p>
                        </div>

                        <div className="content">
                            <form onSubmit={this.handleSubmit}>

                                <input id="email" type="email" name="email" value={this.state.email} onChange={this.handleChange} title="email" placeholder="Email" required
                                       autoFocus />
                                    <input id="password" type="password" value={this.state.password} onChange={this.handleChange} name="password" title="password"
                                           placeholder="Contraseña" required />

                                        <div className="level options">
                                            <div className="checkbox level-left">
                                                <input type="checkbox" id="checkbox" className="regular-checkbox" />
                                                    <label htmlFor="checkbox"></label>
                                                    <span>Recuerdame</span>
                                            </div>

                                            <Link className="is-link has-text-centered" to="/registro">¿No tienes cuenta? Registrate!</Link>
                                        </div>

                                        <button type="submit" className="btn btn-primary">Iniciar Sesión</button>
                            </form>
                        </div>
                    </div>
                </div>
        )


    }



}
function mapStateToProps(state) {
    return {
        logged: state.userLogged,
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({userLogged:userLogged,userNotLogged:userNotLogged}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Login);