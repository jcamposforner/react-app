import React,{Component} from 'react';
import axios from "axios";


class Register extends Component {
    constructor(props){
        super(props)

        this.state = {
            isLoading: false,
            name: '',
            nick: '',
            surname: '',
            email: '',
            password: '',
            token: localStorage.getItem('jwtToken'),
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount(){
        console.log(this.state)
    }

    handleChange(event) {
        this.setState({
            name: event.target.form.name.value,
            nick: event.target.form.nick.value,
            surname: event.target.form.surname.value,
            email: event.target.form.email.value,
            password: event.target.form.password.value,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ isLoading: true });

        console.log(this.state)

        axios.post('http://localhost:3800/api/register',this.state).then(
            (res) => {
                console.log(res.data.message);
                if(res.data.message){
                    this.setState({
                        registered: res.data.message,
                    });
                    return false;
                }
                localStorage.setItem('jwtToken',res.data.token);
                this.setState.registered = true;
            },
            (err) => console.log('Error autenticación')
        );

        // this.props.login(this.state).then(
        //     (res) => this.context.router.push('/'),
        //     (err) => this.setState({ isLoading: false })
        // );


    }



    render() {
        return (
            <div id="login">

                <div className="login-card">
                    {this.state.registered ?

                    <div className="notification is-danger">
                        {this.state.registered}
                    </div>
                    : null}
                    <div className="card-title">
                        <h1>Registrarse</h1>
                    </div>

                    <div className="content">
                        <form onSubmit={this.handleSubmit}>

                            <input id="name" type="text" name="name" value={this.state.name} onChange={this.handleChange} title="name" placeholder="Nombre" required
                                   autoFocus />
                            <input id="surname" type="text" name="surname" value={this.state.surname} onChange={this.handleChange} title="surname" placeholder="Apellidos" required
                                   autoFocus />
                            <input id="email" type="email" value={this.state.email} onChange={this.handleChange} name="emailRegister" title="email"
                                   placeholder="Email" required />
                            <input id="nick" type="text" value={this.state.nick} onChange={this.handleChange} name="nick" title="nick"
                                   placeholder="Nick" required />
                            <input id="password" type="password" value={this.state.password} onChange={this.handleChange} name="password" title="password"
                                   placeholder="Contraseña" required />

                            <button type="submit" className="btn btn-primary">Registrarse</button>
                        </form>
                    </div>
                </div>
            </div>
        )


    }



}

export default Register;