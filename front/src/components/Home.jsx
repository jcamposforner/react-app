import React,{Component} from 'react';
import Products from "./Home/Products";
import Carousel from './Home/Carousel';
import Category from './Home/Category';
import About from './Home/About';
import axios from "axios";

class Shop extends Component {
    constructor(props){
        super(props)

        this.state = {
            categories:[],
            category: ['1 Categoria','2 Categoria','3 Categoria', '4 Categoria'],
            courses: ['asda','asdsa','dasds','dasds','dasds','asdsa'],
        }



    }
    componentDidMount(){
        axios.get(`http://localhost:3800/api/categories`).then(res=>{
            this.setState({
                categories:res.data.category
            })
        })
    }


    render() {


        return (
            <div className="container">

                {/*<Carousel></Carousel>*/}

                <h1 className="title titleMargin">Tu tienda Online</h1>
                <h2 className="subtitle">Categorías</h2>
                <Category categories={this.state.categories}></Category>

                <h2 className="subtitle">Productos Destacados</h2>
                <Products courses={this.state.courses}></Products>

                <h2 className="subtitle">Más vendidos</h2>
                <Products courses={this.state.courses}></Products>

                <h2 className="subtitle">¿Quienes Somos?</h2>
                <About></About>
            </div>
        )



    }



}

export default Shop;