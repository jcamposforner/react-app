import React from 'react';


const Products = (props) => {
    // Cada 4 nueva columna
    const rows = [...Array( Math.ceil(props.courses.length / 6) )];

    const productRows = rows.map( (row, idx) => props.courses.slice(idx * 6, idx * 6 + 6) );

    const content = productRows.map((row, idx) => (
        <div className="columns products-home" key={idx}>
            { row.map( product =>
                <div key={product} className="column is-2">
                    <div className="card">
                        <div className="card-image">
                            <figure className="image is-4by3">

                                <img src="https://bulma.io/images/placeholders/1280x960.png"
                                     alt="Placeholder image" />
                            </figure>
                        </div>
                        <div className="card-content">
                            <div className="content">
                                {product}
                            </div>
                        </div>
                    </div>
                </div> )}
        </div> )
    );
    return(
        <div>
            {content}
        </div>
    )

}

export default Products;