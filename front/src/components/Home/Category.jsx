import React from 'react';
import {Link} from "react-router-dom";


const Category = (props) => {
    // Cada 4 nueva columna
    const rows = [...Array( Math.ceil(props.categories.length / 2) )];

    const productRows = rows.map( (row, idx) => props.categories.slice(idx * 2, idx * 2 + 2) );

    const content = productRows.map((row, idx) => (
        <div className="columns products-home" key={idx}>
            { row.map( product =>
                <div key={product} className="column is-6">
                    <Link to={"/"+product.slug}>
                    <div className="card">
                        <div className="card-image">
                            <figure className="image is-4by3">

                                <img src="https://bulma.io/images/placeholders/1280x960.png"
                                     alt={product._id} />
                            </figure>
                        </div>
                        <div className="card-content">
                            <div className="content">
                                {product.family}
                            </div>
                        </div>
                    </div>
                    </Link>
                </div> )}
        </div> )
    );
    return(
        <div>
            {content}
        </div>
    )

}

export default Category;