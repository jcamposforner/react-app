import React,{Component} from 'react';
import { Slide } from 'react-slideshow-image';

class Carousel extends Component {
    constructor(props){



    }



    render() {
        const slideImages = [
            '1.png',
            'asas.jpg',
            'asas.jpg'
        ];

        const properties = {
            duration: 5000,
            transitionDuration: 500,
            infinite: true,
            indicators: true,
            arrows: true
        }

        return (
            <Slide {...properties}>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[0]})`}}>
                        <span>Slide 1</span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[1]})`}}>
                        <span>Slide 2</span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[2]})`}}>
                        <span>Slide 3</span>
                    </div>
                </div>
            </Slide>
        );



    }



}

export default Carousel;