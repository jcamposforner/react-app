import React,{Component} from 'react';
import ProductCart from './product';
import StartCart from './realizarpedido';

class Cart extends Component {
    constructor(props){
        super(props)


        this.state = {
            count: 0,
            products: [''],
        }

    }

    componentDidMount(){
        var productsStorage = JSON.parse(localStorage.getItem('cart'));
        var countProducts = productsStorage.length;
        this.setState({
            products: productsStorage,
            count: countProducts,
        })
    }

    getTotal(){
        var totalArr = [];
        this.state.products.forEach(function (product) {
            totalArr.push(product.price);
        })

        var total = totalArr .reduce((a,b) => a + b, 0);
        return total;
    }

    render() {

        return (
            <div id="margin">
                <div className="container">


                    <div className="columns">
                        <div className="column is-8">
                            <h2 className="subtitle has-text-centered-mobile">Cesta ( {this.state.count} artículos)</h2>
                            <hr/>

                            {
                                this.state.products.map(product => (
                                    <ProductCart product={product}></ProductCart>
                                ))
                            }

                        </div>
                        <StartCart total={this.getTotal()}></StartCart>
                    </div>

                </div>
            </div>
        )



    }



}

export default Cart;