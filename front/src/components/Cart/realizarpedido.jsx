import React,{Component} from 'react';
import {Link} from "react-router-dom";
class StartCart extends Component {
    constructor(props){
        super(props)



    }

    render() {

        return (
            <div className="column is-4">
                <h2 className="subtitle has-text-centered-mobile">Total ( {this.props.total} € )</h2>
                <hr/>
                <div className="card">
                    <div className="card-content">
                        <div className="column is-12">
                            <div className="columns">
                                <div className="column is-7">
                                    Entrega
                                </div>

                                <div className="column is-5">
                                    <b>Próximo día laborable</b>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="column is-12">
                            <div className="columns">
                                <div className="column is-7">
                                    Gastos de envío
                                </div>

                                <div className="column is-5">
                                    <b>GRATIS</b>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="column is-12">
                            <div className="columns">
                                <div className="column is-7">
                                    Total (IVA incluido)
                                </div>

                                <div className="column is-5">
                                    <b>{this.props.total} €</b>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <Link to="/checkout">
                            <button className="button is-link">Realizar pedido</button>
                        </Link>
                    </div>
                </div>
            </div>
        )



    }



}

export default StartCart;