import React,{Component} from 'react';

class ProductCart extends Component {
    constructor(props){
        super(props)


    }

    render() {
        console.log(this.props)
        return (
            <div className="card">
                <div className="card-content">
                    <div className="columns">
                        <div className="column is-4">
                            <figure className="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png"
                                     alt={this.props.product._id} />
                            </figure>
                        </div>
                        <div className="column is-8">
                            <p><b>{this.props.product.title}</b></p>
                            <p>{this.props.product.shortdescription}</p>
                            <p>{this.props.product.price} €</p>
                        </div>
                    </div>
                </div>
                <hr/>
            </div>

        )



    }



}

export default ProductCart;