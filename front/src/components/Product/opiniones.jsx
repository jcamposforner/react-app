import React,{Component} from 'react';
import axios from "axios";
import Moment from 'react-moment'

class Opiniones extends Component {
    constructor(props){
        super(props)
        axios.get(`http://localhost:3800/api/comment/${this.props.match}`).then(res=>{
            if(!res) return false;
            if(res.data.comments){
                this.setState({
                    comments: res.data.comments,
                    page: res.data.page,
                    pages: res.data.pages,
                    total: res.data.total,
                })
            }
        })

        this.state = {
            comments:[],
            page: Number,
            pages: Number,
            total: Number,
            nocomments: 'Todavía no hay comentarios ¡Vuelve más tarde!'
        }

    }

    componentWillReceiveProps(nextProps) {
        axios.get(`http://localhost:3800/api/comment/${nextProps.match}`).then(res => {
            if (!res) return false;
            if (res.data.comments) {
                this.setState({
                    comments: res.data.comments,
                    page: res.data.page,
                    pages: res.data.pages,
                    total: res.data.total,
                })
            }
        })

    }

    render() {
        return (

                <div className="columns">
                    <div className="column is-6">
                        <h2 className="subtitle">Opiniones de los usuarios</h2>
                        <hr/>
                        {
                            (this.state.comments) ?
                            this.state.comments.map((comment)=>
                                <div className="card">
                                    <div className="card-content">
                                        <div className="media">
                                            <div className="media-left">
                                                <figure className="image is-64x64">
                                                    <img src={"/uploads/user/"+comment.user.image}
                                                         alt={comment._id} />
                                                </figure>
                                            </div>
                                            <div className="media-content">
                                                <p className="title is-4">{comment.user.name} {comment.user.surname}</p>
                                                <p className="subtitle is-6">{comment.user.nick} - RATING</p>

                                            </div>
                                        </div>
                                        <div className="content">
                                            <div dangerouslySetInnerHTML={{__html: comment.body}}></div>
                                            <br />
                                            <small><Moment format="DD/MM/YYYY"><i>{comment.created_at.dateToFormat}</i></Moment></small>
                                        </div>
                                    </div>
                                    <hr />
                                </div>


                            ) : <h2 className="subtitle is-danger">{this.state.nocomments}</h2>



                        }



                    </div>
                    <div className="column is-6">

                    </div>

                </div>

        )



    }



}

export default Opiniones;