import React,{Component} from 'react';
import { Fade } from 'react-slideshow-image';
import axios from "axios";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {addToCart} from "../../actions";

class ProductMain extends Component {
    constructor(props){
        super(props)

        this.state = {
            product:[''],
            cartComp: (this.props.cart) ? this.props.cart : null,
        }

        this.addToCart = this.addToCart.bind(this);
    }


    addToCart(){
        console.log(JSON.parse(localStorage.getItem('cart')))
        this.props.addToCart(this.state.product[0]);
    }

    componentWillMount(){
        window.scrollTo(0, 0);

        axios.get(`http://localhost:3800/api/product/${this.props.match}`).then(res=>{
            if(!res) return false;
            if(res.data.product){
                this.setState({
                    product: res.data.product,
                })
            }
        })
    }
    componentWillReceiveProps(nextProps) {
        window.scrollTo(0, 0);
        axios.get(`http://localhost:3800/api/products/${nextProps.match}`).then(res => {
            if (!res) return false;
            if (!res) return false;
            if (res.data.product) {
                this.setState({
                    product: res.data.product,
                })
            }
        })

    }

    render() {
        console.log(this.props)
        const fadeImages = [
            'https://bulma.io/images/placeholders/1280x960.png',
            'https://bulma.io/images/placeholders/1280x960.png',
            'https://bulma.io/images/placeholders/1280x960.png'
        ];

        const fadeProperties = {
            duration: 5000,
            transitionDuration: 500,
            infinite: true,
            indicators: false
        }


        console.log(this.props)
        return (

            <div className="columns">


                <div className="column is-6">
                    <div className="card">
                        <Fade {...fadeProperties}>
                            <div className="each-fade">
                                <div className="image-container">
                                    <img src={fadeImages[0]} />
                                </div>
                            </div>
                            <div className="each-fade">
                                <div className="image-container">
                                    <img src={fadeImages[1]} />
                                </div>

                            </div>
                            <div className="each-fade">
                                <div className="image-container">
                                    <img src={fadeImages[2]} />
                                </div>

                            </div>
                        </Fade>
                        <div className="card-content">
                            <div className="columns">
                                <div className="column is-4">
                                    <img src={fadeImages[0]}
                                         alt="Placeholder image" />
                                </div>
                                <div className="column is-4">
                                    <img src={fadeImages[1]}
                                         alt="Placeholder image" />
                                </div>
                                <div className="column is-4">
                                    <img src={fadeImages[2]}
                                         alt="Placeholder image" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="column is-6 fixed" id="test">
                    <h1 className="title">{this.state.product[0].title}</h1>
                    <h2 className="subtitle">{this.state.product[0].shortdescription}</h2>
                    <p>{this.state.product[0].totalRating} STARS</p>
                    <p><b>{this.state.product[0].price}€</b></p>
                    <hr/>
                    <p dangerouslySetInnerHTML={{__html:this.state.product[0].description}}>



                    </p>
                    <hr/>
                    <div className="columns has-text-centered">
                        <div className="column is-4">
                            <div className="select is-large">
                                <select>
                                    <option value="1">1</option>
                                </select>
                            </div>
                        </div>
                        <div className="column is-8">
                            <button onClick={this.addToCart} className="button is-large is-link">Añadir a la cesta</button>
                        </div>
                    </div>
                </div>

            </div>
        )


    }



}
function mapStateToProps(state) {
    return {
        cart: state.cartReduc,
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({addToCart:addToCart}, dispatch);
}


export default connect(mapStateToProps,matchDispatchToProps)(ProductMain);