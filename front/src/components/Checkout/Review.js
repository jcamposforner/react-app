import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import {connect} from 'react-redux';


const cartProducts = JSON.parse(localStorage.getItem('cart'));

const products = [
    { name: 'Product 1', desc: 'A nice thing', price: '$9.99' },
    { name: 'Product 2', desc: 'Another thing', price: '$3.45' },
    { name: 'Product 3', desc: 'Something else', price: '$6.51' },
    { name: 'Product 4', desc: 'Best thing of all', price: '$14.11' },
    { name: 'Shipping', desc: '', price: 'Free' },
];
const addresses = ['1 Material-UI Drive', 'Reactville', 'Anytown', '99999', 'USA'];
const payments = [
    { name: 'Card type', detail: 'Visa' },
    { name: 'Card holder', detail: 'Mr John Smith' },
    { name: 'Card number', detail: 'xxxx-xxxx-xxxx-1234' },
    { name: 'Expiry date', detail: '04/2024' },
];

const styles = theme => ({
    listItem: {
        padding: `${theme.spacing.unit}px 0`,
    },
    total: {
        fontWeight: '700',
    },
    title: {
        marginTop: theme.spacing.unit * 2,
    },
});
function getTotal(){
    var totalArr = [];
    cartProducts.forEach(function (product) {
        totalArr.push(product.price);
    })

    var total = totalArr .reduce((a,b) => a + b, 0);
    return total;
}
function Review(props) {
    const { classes } = props;
    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Productos
            </Typography>
            <List disablePadding>
                {cartProducts.map(product => (
                    <ListItem className={classes.listItem} key={product.name}>
                        <ListItemText primary={product.title} secondary={product.shortdescription} />
                        <Typography variant="body2">{product.price} €</Typography>
                    </ListItem>
                ))}
                <ListItem className={classes.listItem}>
                    <ListItemText primary="Total" />
                    <Typography variant="subtitle1" className={classes.total}>
                        {getTotal()} €
                    </Typography>
                </ListItem>
            </List>
            <Grid container spacing={16}>
                <Grid item xs={12} sm={6}>
                    <Typography variant="h6" gutterBottom className={classes.title}>
                        Dirección de envio
                    </Typography>
                    {/*
                    ShippingAddress:
                        CP: "d"
                        address1: "d"
                        address2: "d"
                        city: "d"
                        country: "d"
                        firstName: "d"
                        lastName: "d"
                    */}
                    <Typography gutterBottom>{props.Shippment.ShippingAddress.firstName} {props.Shippment.ShippingAddress.lastName}</Typography>
                    <Typography gutterBottom>{props.Shippment.ShippingAddress.address1},{props.Shippment.ShippingAddress.city}, {props.Shippment.ShippingAddress.country},{props.Shippment.ShippingAddress.cp}</Typography>
                </Grid>
                <Grid item container direction="column" xs={12} sm={6}>
                    <Typography variant="h6" gutterBottom className={classes.title}>
                        Detalles del Pago
                    </Typography>
                    {/*

                    {/*
                    PaymentMethod:
                        cardName: "d"
                        cardNumber: "d"
                        cvv: "d"
                        expDate: "d"
                    */}
                    <Grid container>
                        <React.Fragment key={props.Shippment.PaymentMethod.cardName}>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom>Nombre de la Tarjeta:</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom><b>{props.Shippment.PaymentMethod.cardName}</b></Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom>Número de la Tarjeta:</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom><b>{props.Shippment.PaymentMethod.cardNumber}</b></Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom>CVV:</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom><b>{props.Shippment.PaymentMethod.cvv}</b></Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom>Fecha de expiración:</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography align={"center"} gutterBottom><b>{props.Shippment.PaymentMethod.expDate}</b></Typography>
                            </Grid>
                        </React.Fragment>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );

}

Review.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    console.log()
    return {
        Shippment: state.Shipping,
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Review));