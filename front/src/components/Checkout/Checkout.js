import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review';
import {Link} from "react-router-dom";
import {ShippingAddressAction , PaymentMethodAction} from '../../actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Elements, StripeProvider} from 'react-stripe-elements';




const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
        padding: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
            marginTop: theme.spacing.unit * 6,
            marginBottom: theme.spacing.unit * 6,
            padding: theme.spacing.unit * 3,
        },
    },
    stepper: {
        padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing.unit * 3,
        marginLeft: theme.spacing.unit,
    },
});

const steps = ['Dirección de envio', 'Método de Pago', 'Tu compra'];

function getStepContent(step) {
    switch (step) {
        case 0:
            return <AddressForm />;
        case 1:
            return <PaymentForm />;
        case 2:
            return <Review />;
        default:
            throw new Error('Unknown step');
    }
}

const cartProducts = JSON.parse(localStorage.getItem('cart'));

function getTotal(){
    var totalArr = [];
    cartProducts.forEach(function (product) {
        totalArr.push(product.price);
    })

    var total = totalArr .reduce((a,b) => a + b, 0);
    return total;
}
class Checkout extends React.Component {
    state = {
        activeStep: 0,
    };


    ShippingAddress(){
        const ShippingAddress = {
            firstName: document.getElementById("firstName").value,
            lastName: document.getElementById("lastName").value,
            address1: document.getElementById("address1").value,
            address2: document.getElementById("address2").value,
            city: document.getElementById("city").value,
            CP: document.getElementById("zip").value,
            country: document.getElementById("country").value,
        };
        if(ShippingAddress.firstName && ShippingAddress.lastName
            && ShippingAddress.address1 && ShippingAddress.city
            && ShippingAddress.CP){
            this.props.ShippingAddressAction(ShippingAddress)
            return true;
        }else{
            return false
        }
    }

    PaymentMethod(){
        const Payment = {
            cardName: document.getElementById("cardName").value,
            cardNumber: document.getElementById("cardNumber").value,
            expDate: document.getElementById("expDate").value,
            cvv: document.getElementById("cvv").value,
        };
        if(Payment.cardName && Payment.cardNumber
            && Payment.expDate && Payment.cvv){
            this.props.PaymentMethodAction(Payment)
            return true;
        }else{
            return false
        }
    }

    handleNext = () => {
        if(this.state.activeStep === 0){
            if(this.ShippingAddress() === true){
                this.setState(state => ({
                    activeStep: state.activeStep + 1,
                }));
            }
        }else if(this.state.activeStep === 1){
            if(this.PaymentMethod() === true) {
                this.setState(state => ({
                    activeStep: state.activeStep + 1,
                }));
            }
        }else {
            this.setState(state => ({
                activeStep: state.activeStep + 1,
            }));
        }

    };


    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1,
        }));
    };

    buyProducts = () => {
        const PaymentMethod = this.props.Shippment.PaymentMethod;
        const ShippingAddress = this.props.Shippment.ShippingAddress;
        console.log(getTotal()*100);
        let response = fetch("http://localhost:3800/api/charge", {
            method: "POST",
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
            chargeAmount: getTotal()*100,
            stripeToken:'tok_ca',
        });
        console.log(response)
    }

    handleReset = () => {
        this.setState({
            activeStep: 0,
        });
    };

    render() {
        const { classes } = this.props;
        const { activeStep } = this.state;

        return (
            <React.Fragment>
                <CssBaseline />
                <div id="margin"></div>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h4" align="center">
                            Carrito
                        </Typography>
                        <Stepper activeStep={activeStep} className={classes.stepper}>
                            {steps.map(label => (
                                <Step key={label}>
                                    <StepLabel>{label}</StepLabel>
                                </Step>
                            ))}
                        </Stepper>
                        <React.Fragment>
                            {activeStep === steps.length ? (
                                <React.Fragment>
                                    <Typography variant="h5" gutterBottom>
                                        Gracias por comprar.
                                    </Typography>
                                    <Typography variant="subtitle1">
                                        Tu pedido es el #2001539.
                                    </Typography>
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                    {getStepContent(activeStep)}
                                    <div className={classes.buttons}>
                                        {activeStep !== 0 && (
                                            <Button onClick={this.handleBack} className={classes.button}>
                                                Back
                                            </Button>
                                        )}
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={activeStep === steps.length - 1 ? this.buyProducts :this.handleNext}
                                            className={classes.button}
                                        >
                                            {activeStep === steps.length - 1 ? 'Comprar' : 'Siguiente'}
                                        </Button>
                                    </div>
                                </React.Fragment>
                            )}
                        </React.Fragment>
                    </Paper>
                </main>
            </React.Fragment>
        );
    }
}

Checkout.propTypes = {
    classes: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
    console.log(state.Shipping)
    return {
        Shippment: state.Shipping,
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({ShippingAddressAction:ShippingAddressAction,PaymentMethodAction:PaymentMethodAction}, dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Checkout));