import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {CardElement, injectStripe, Elements} from 'react-stripe-elements';

function PaymentForm(props) {



    return (

        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Método de Pago
            </Typography>
            <Grid container spacing={24}>
                <Grid item xs={12} md={6}>
                    <TextField required id="cardName" label="Nombre de tarjeta" fullWidth />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField required id="cardNumber" label="Numero de tarjeta" fullWidth />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField required id="expDate" label="Fecha de expiración" fullWidth />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cvv"
                        label="CVV"
                        helperText="Últimos 3 digitos de la tarjeta"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={<Checkbox color="secondary" name="saveCard" value="yes" />}
                        label="Recordar esta tarjeta para mas ocasiones"
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

export default PaymentForm;