import React,{Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import NavBarItems from './items';
import axios from 'axios';
import { store }from '../../index'
import Badge from "@material-ui/core/Badge/Badge";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";



class NavBar extends Component {
    constructor(props){
        super(props)

        this.state = {
            categories: [],
            countProducts: (localStorage.getItem('quantity')) ? localStorage.getItem('quantity') : 0,
            isLogged: (this.props.logged) ? this.props.logged : false,
            token: localStorage.getItem('jwtToken'),
            identity: JSON.parse(localStorage.getItem('identity')),
        }
        store.subscribe(() =>{
            console.log(localStorage.getItem('quantity'));
                this.setState({
                    countProducts: localStorage.getItem('quantity'),
                })
            })
        }

    componentDidMount(){
        axios.get(`http://localhost:3800/api/categories`).then(res=>{
            this.setState({
                categories:res.data.category
            })
        })
    }


    render() {
        return (

            <div>
                <nav className="navbar custom box-shadow is-primary is-fixed-top" role="navigation" aria-label="main navigation">
                    <div>

                        <div className="navbar-brand">
                            <Link className="navbar-item" to="/">Home { this.props.logged ? this.props.logged.payload ? this.props.logged.payload.name : null : null }
                            { this.state.identity ? this.state.identity.name : null}</Link>

                            {this.state.categories.map(category =>
                                <NavBarItems category={category}></NavBarItems>
                            )}

                            {  this.state.identity ?
                                <Link className="navbar-item" to="/login">
                                    <i className="fas fa-user-check"></i>
                                </Link>:
                                <Link className="navbar-item" to="/login">
                                    <i className="fas fa-user"></i>
                                </Link>
                            }

                            <Link className="navbar-item" to="/carro">

                                <Badge badgeContent={this.state.countProducts} color="secondary">
                                    <ShoppingBasket />
                                </Badge>

                            </Link>
                        </div>
                    </div>

                </nav>



                <div className="buscador is-flex is-light">
                    <div className="column has-text-centered is-4">
                        <Link to="/">LOGO</Link>
                        <ul>

                        </ul>
                    </div>
                    <div className="column has-text-centered  is-4">
                        BUSCADOR
                    </div>
                    <div className="column has-text-centered is-4">
                        TELEFONO
                    </div>
                </div>
            </div>
        );



    }



}

function mapStateToProps(state) {
    return {
        logged: state.userLogged,
        cart:state.cartReduc,
    }
}


export default connect(mapStateToProps)(NavBar);