import React,{Component} from 'react';
import {Link} from "react-router-dom";

class NavBarUser extends Component {
    constructor(props){
        super(props)

    }

    render() {

        return (

            <div>
                <Link className="navbar-item" to="/login">
                    <i className="fas fa-user"></i>
                </Link>
                <Link className="navbar-item" to="/carro">
                    <i className="fas fa-shopping-cart"></i>
                </Link>
            </div>

        );



    }



}

export default NavBarUser;