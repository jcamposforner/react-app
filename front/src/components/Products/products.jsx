import React,{Component} from 'react';
import {Link} from "react-router-dom";

class ProductsSubcategory extends Component {
    constructor(props){
        super(props)

    }



    render() {

        // Cada 4 nueva columna
        const rows = [...Array( Math.ceil(this.props.products.length / 4) )];

        const productRows = rows.map( (row, idx) => this.props.products.slice(idx * 4, idx * 4 + 4) );

        const content = productRows.map((row, idx) => (
            <div className="columns products-home" key={idx}>
                { row.map( product =>
                    <div key={product._id} className="column is-3">
                        <Link to={"/producto/"+product.slug}>
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is-4by3">

                                        <img src="https://bulma.io/images/placeholders/1280x960.png"
                                             alt={product._id} />
                                    </figure>
                                </div>
                                <div className="card-content">
                                    <div className="content">
                                        <div className="columns">
                                            <div className="column is-8">
                                                {product.title}
                                            </div>
                                            <div className="column is-4">
                                                <b> {product.price} €</b>
                                            </div>
                                        </div>
                                        {/* Contenido */}
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </div> )}
            </div> )
        );

        return (
            <div>

                {content}

            </div>
        )



    }



}

export default ProductsSubcategory;