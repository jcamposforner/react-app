import React,{Component} from 'react';
import axios from "axios";
import ProductsSubcategory from './products';
import {Link} from "react-router-dom";

class Products extends Component {
    constructor(props){
        super(props)

        this.state = {
            products:[],
            page: 1,
        }


    }

    // Para cuando actualicemos en la ruta :category
    componentWillReceiveProps(nextProps){
        window.scrollTo(0, 0);
        if(nextProps.match.page){
            axios.get(`http://localhost:3800/api/products/${nextProps.match.subcategory}/${nextProps.match.page}`).then(res=>{
                if(!res) return false;
                if(res.data.products){
                    this.setState({
                        page: res.data.page,
                        pages: res.data.pages,
                        totalProducts: res.data.total,
                        products: res.data.products,
                    })
                }
            })
        }else{
            axios.get(`http://localhost:3800/api/products/${nextProps.match.subcategory}/${nextProps.match.page}`).then(res=>{
                if(!res) return false;
                if(res.data.products){
                    this.setState({
                        page: 1,
                        pages: res.data.pages,
                        totalProducts: res.data.total,
                        products: res.data.products,
                    })
                }
            })
        }

    }

    // Para cuando se entra por primera vez en la ruta :category
    componentWillMount(){
        window.scrollTo(0, 0);
        if(this.props.match.page){
            axios.get(`http://localhost:3800/api/products/${this.props.match.subcategory}/${this.props.match.page}`).then(res=>{
                if(!res) return false;
                if(res.data.products){
                    this.setState({
                        page: res.data.page,
                        pages: res.data.pages,
                        totalProducts: res.data.total,
                        products: res.data.products,
                    })
                }
            })
        }else{
            axios.get(`http://localhost:3800/api/products/${this.props.match.subcategory}/${this.props.match.page}`).then(res=>{
                if(!res) return false;
                if(res.data.products){
                    this.setState({
                        page: 1,
                        pages: res.data.pages,
                        totalProducts: res.data.total,
                        products: res.data.products,
                    })
                }
            })
        }
    }


    createElements(){
        var elements = [];
        var url=[];
        url.push(this.props.location.split('/'));
        for(let i = 1; i < this.state.pages+1; i++){
            if(i == this.state.page){
                elements.push(
                    <li>
                        <Link to={"/"+url[0][1]+"/"+url[0][2]+"/"+i} className="pagination-link is-current" aria-current="page" aria-label="Current Page">{i}</Link>
                    </li>
                );
            }else{
                elements.push(
                    <li>
                        <Link to={"/"+url[0][1]+"/"+url[0][2]+"/"+i} className="pagination-link" aria-current="page" aria-label={"Goto page "+i}>{i}</Link>
                    </li>
                );
            }
        }
        return elements;
    }

    createPaginator(){
        var paginator = [];
        var url=[];
        url.push(this.props.location.split('/'));

        var StringToInt = parseInt(this.state.page,10);
        var PrevPage = StringToInt-1;
        var NextPage = StringToInt+1;

        if(this.state.page != 1){
            paginator.push(<Link className="pagination-previous" to={"/"+url[0][1]+"/"+url[0][2]+"/"+PrevPage}>Anterior</Link>);
        }
        if(this.state.pages != this.state.page){
            paginator.push(<Link className="pagination-previous" to={"/"+url[0][1]+"/"+url[0][2]+"/"+NextPage}>Siguiente</Link>)
        }
        return paginator;
    }

    render() {

        return (
            <div id="margin">
                <div className="container">

                    <h2 className="title">Productos de {this.props.match.subcategory} ( {this.state.totalProducts} productos )</h2>

                    <ProductsSubcategory products={this.state.products}></ProductsSubcategory>

                    <hr/>


                    <nav className="pagination is-medium" role="navigation" aria-label="pagination">

                        {this.createPaginator()}

                        <ul className="pagination-list">
                            {this.createElements()}

                        </ul>
                    </nav>

                </div>
            </div>
        )



    }



}

export default Products;