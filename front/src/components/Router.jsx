import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter,Switch } from 'react-router-dom';
import NavBar from './header/navbar';
import Login from './login';
import Shop from './Home';
import Register from './register';
import Product from './Product/index';
import Products from './Products/index';
import Family from './Family/index';
import Footer from './footer';
import Checkout from './Checkout/Checkout'
import Dashboard from './Dashboard/Dashboard';
import Cart from './Cart/index';
import {StripeProvider, Elements} from 'react-stripe-elements';



// PARA QUE NO TENGA HEADER EL ADMIN
const DefaultRouter = () => (
    <Router>
        <Switch>
            <Route exact path="/admin" component={DashboardComponent} />
            <Route component={StaticSite} />
        </Switch>
    </Router>
)

const DashboardComponent = () => (
    <Dashboard></Dashboard>
)

const StaticSite = () => (

            <Router>
                <div>
                    <NavBar></NavBar>
                    <Switch>
                        <Route exact path="/" component={HomeShop} />
                        <Route exact path="/login" component={LoginComponent} />
                        <Route exact path="/registro" component={RegisterComponent} />
                        <Route exact path="/checkout" component={CheckoutComponent}/>
                        <Route exact path="/contact" component={Contact} />
                        <Route exact path="/carro" component={CartComponent} />
                        <Route exact path="/producto/:name" name="product" component={ProductComponent} />
                        <Route exact path="/:category" component={FamilyComponent} />
                        <Route exact path="/:category/:subcategory/:page?" component={ProductsComponent} />
                        <Route exact component={notFound} />
                    </Switch>
                    <Footer></Footer>

                </div>

            </Router>
)

const HomeShop = () => (
    <Shop></Shop>
)

const FamilyComponent = ({ match,location }) => (
    <Family match={match.params.category} location={location.pathname}></Family>
)

const ProductsComponent = ({match, location}) => (
    <Products match={match.params} location={location.pathname}></Products>
)

const CartComponent = () => (
    <Cart></Cart>
)

const ProductComponent = ({ match }) => (
    <div>
        <Product match={match.params.name}></Product>
    </div>
)

const LoginComponent = () => (
    <Login></Login>
)

const RegisterComponent = () => (
    <Register></Register>
)

const Contact = () => (
    <div>
        <h2>CONTACT</h2>
    </div>
)

const CheckoutComponent = ({ location }) => (
    <StripeProvider apiKey="pk_test_ZQWVgtLXoepwCgJfWNxGRIBr">

            <Checkout></Checkout>
    </StripeProvider>
)

const notFound = ({ location }) => (
    <div>
        <h3>No match for <code>{location.pathname}</code></h3>
    </div>
)

export default DefaultRouter;