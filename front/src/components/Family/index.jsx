import React,{Component} from 'react';
import axios from 'axios';
import SubFamily from './subfamilies'

class Family extends Component {
    constructor(props){
        super(props)


        this.state = {
            subcategories:[],
        }



    }

    // Para cuando actualicemos en la ruta :category
    componentWillReceiveProps(nextProps){
        axios.get(`http://localhost:3800/api/subfamily/${nextProps.match}`).then(res=>{
            if(!res) return false;
            if(res.data.subfamilies){
                this.setState({
                    subcategories: res.data.subfamilies
                })
            }
        });
    }

    // Para cuando se entra por primera vez en la ruta :category
    componentWillMount(){
        axios.get(`http://localhost:3800/api/subfamily/${this.props.match}`).then(res=>{
            if(!res) return false;
            if(res.data.subfamilies){
                this.setState({
                    subcategories: res.data.subfamilies
                })
            }
        })
    }


    render() {

        return (
            <div id="margin">
                <div className="container">

                    <h2 className="title">Subcategorías de {this.props.match}</h2>
                    <SubFamily match={this.props.match} courses={this.state.subcategories}></SubFamily>
                </div>
            </div>
        )



    }



}

export default Family;