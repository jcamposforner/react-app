import React,{Component} from 'react';
import {Link} from "react-router-dom";
import { store }from '../../index'
import axios from "axios";

class SubFamily extends Component {
    constructor(props){
        super(props)

        this.state = {
            subcategories: [],
        }

    }


    render() {
        const rows = [...Array( Math.ceil(this.props.courses.length / 3) )];

        const productRows = rows.map( (row, idx) => this.props.courses.slice(idx * 3, idx * 3 + 3) );
        const content = productRows.map((row, idx) => (
            <div className="columns products-home" key={idx}>
                { row.map( subfamily =>
                    <div key={subfamily._id} className="column is-4">
                        <Link to={"/"+this.props.match+"/"+subfamily.slug}>
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is-4by3">

                                        <img src="https://bulma.io/images/placeholders/1280x960.png"
                                             alt={subfamily._id} />
                                    </figure>
                                </div>
                                <div className="card-content">
                                    <div className="content">
                                        {subfamily.subfamily}
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </div> )}
            </div> )
        );
        return (
            <div>
                {content}
            </div>
        )



    }



}

export default SubFamily;