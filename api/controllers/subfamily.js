'use strict'



// Primera letra mayuscula para saber que es un modelo
var Family = require('../models/family');
var SubFamily = require('../models/subfamily');
var jwt = require('../services/jwt');

function saveSubFamily(req,res) {
    var params = req.body;

    if(params.subfamily && params.family && params.slug){

        var subfamily = new SubFamily();
        subfamily.subfamily = params.subfamily;
        subfamily.family = params.family;
        subfamily.slug = params.slug;
        subfamily.activated = (params.activated) ? params.activated : false;


        SubFamily.countDocuments({subfamily:params.subfamily}).exec((err,countSubFamily)=>{
            if(countSubFamily > 0) return res.status(404).send({message:'Ya existe esa subfamilia'});
            if(err) return res.status(500).send({message:'Ha ocurrido un error'});
            subfamily.save((err,subfamilyStored)=>{
                if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el producto'});

                if(subfamilyStored){
                    res.status(200).send({product: subfamilyStored});
                }else{
                    res.status(404).send({message:'No se ha registrado el producto'});
                }
            });

        });




    }else {
        res.status(200).send({
            message: 'Rellena todos los datos',
        });
    }


}


function getSubCategoriesByCategory(req,res){
    var category = req.params.slug;

    Family.find({slug:category}).exec((err,category)=>{
        if(err) return res.status(500).send({message:'Ha ocurrido un error con tu petición'});
        if(!category) return res.status(404).send({message:'No existe la categoría'});
        if(category[0]){
            SubFamily.find({family:category[0]._id}).exec((err,subfamilies)=>{
                if(err) return res.status(500).send({message:'Ha ocurrido un error con tu petición'});
                if(!subfamilies) return res.status(404).send({message:'No existe las subcategorías'});

                return res.status(200).send({
                    subfamilies
                })
            });
        }else{
            return res.status(200).send({
                message:'No existe la categoria que has pasado'
            })
        }
    });

}


module.exports = {
    saveSubFamily,
    getSubCategoriesByCategory
};