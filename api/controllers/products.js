'use strict'

var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var moment = require('moment');




// Primera letra mayuscula para saber que es un modelo
var Product = require('../models/product');
var SubFamily = require('../models/subfamily');
var Comments = require('../models/comments');
var Family = require('../models/family');
var jwt = require('../services/jwt');



function home(req,res){
    res.status(200).send({
        message: 'Product express'
    });
}


function saveProduct(req,res){
    //Campos de POST
    var params = req.body;

    if(req.user.role !== 'ROLE_ADMIN'){
        // Creamos nuevo usuario
        var product = new Product();

        if(params.title && params.description && params.price && params.slug){

            product.title = params.title;
            product.description = params.description;
            product.price = params.price;
            product.slug = params.slug;
            product.subfamily = params.subfamily;
            product.totalRating = 0;
            product.destacado = (params.destacado) ? params.destacado : false;
            product.shortdescription = params.shortdescription;
            product.votes = 0;
            product.image = null;

            // Comprobar articulos duplicados

            Product.find({ $or: [
                    {title: product.title.toLowerCase()},
                    {description:product.description.toLowerCase()}
                ]}).exec((err,products) => {
                if(err) return res.status(500).send({message:'Error en la petición de producto'});

                if(products && products.length >= 1){
                    return res.status(200).send({message: 'El producto ya existe'});
                }else{
                    product.save((err,productStored) => {
                        if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el producto'});

                        if(productStored){
                            res.status(200).send({product: productStored});
                        }else{
                            res.status(404).send({message:'No se ha registrado el producto'});
                        }
                    });
                }
            });
        }else{
            res.status(200).send({
                message: 'Rellena todos los datos',
            });
        }
    }else{
        return res.status(403).send({message:'No tienes permisos para crear productos'});
    }

}

function getProduct(req,res){
    var productSlug = req.params.slug;

    Product.find({slug:productSlug}).populate('votesUser','-__v -_id').exec((err,product) =>{

        if(err) return res.status(500).send({message:'Error en la petición'});

        if(!product) return res.status(404).send({message:'Producto no existe'});

        return res.status(200).send({product});
    });
}

function getProducts(req,res) {

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Product.find().sort('_id').populate('family votesUser', '-password -role -__v -_id').paginate(page, itemsPerPage, (err, products, total) => {
       if(err) return res.status(500).send({message: 'Error en la petición'});
       if(!products) return res.status(404).send({message: 'No hay productos disponibles'});

       return res.status(200).send({
           products,
           total,
           pages: Math.ceil(total/itemsPerPage),
           page,
       })
    });
}

function saveRating(req,res) {
    var productId = req.params.id;

    var updateRating = parseInt(req.body.totalRating);
    Product.findById(productId).exec((err,product)=>{

        if(product.votesUser.indexOf(req.user.sub)){
            product.votesUser.push(req.user.sub);
            product.totalRating += updateRating;
            product.votes += 1;
            Product.findByIdAndUpdate(productId, product,{new: true},(err,productUpdated) =>{
                if(err) return res.status(500).send({message: 'Error en la petición'});

                if(!productUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

                return res.status(200).send({
                    product: productUpdated,
                });
            });
        }else{
            return res.status(200).send({message:'Ya has votado este producto'});
        }
    });
}

function saveComment(req,res){
    var params = req.body;

    var productId = req.params.id;

    if(!params.body) return res.status(200).send({message:'Envia los campos necesarios'});

    var comment = new Comments();
    comment.user = req.user.sub;
    comment.product = productId;
    comment.body = params.body;
    comment.created_at = moment().unix();

    comment.save((err,commentStored)=>{
        if(err) return res.status(500).send({message:'Error en la petición'});
        if(!commentStored) return res.status(500).send({message:'Error al enviar el comentario'});

        return res.status(200).send({
            comment:commentStored,
        })
    });
}

function getImportantProducts(req,res) {
    Product.find({destacado:true}).exec((err,products)=>{
       if(err) return res.status(500).send({message:'Ha ocurrido un error'});
       if(!products) return res.status(404).send({message:'No hay productos destacados'});

       return res.status(200).send({
          products,
       });
    });
}

function getProductsBySubCategory(req,res) {
    var subcategory = req.params.slug;
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 20;
    SubFamily.find({slug:subcategory}).exec((err,subcategory)=>{
        if(err) return res.status(500).send({message:'Ha ocurrido un error con tu petición'});
        if(!subcategory) return res.status(404).send({message:'No existe la subcategoría'});
        if(subcategory.length>0) {
            Product.find({subfamily: subcategory[0]._id}).sort('-title').paginate(page, itemsPerPage, (err, products, total) => {
                if (err) return res.status(500).send({message: 'Error en la petición'});
                if (!products) return res.status(404).send({message: 'No hay products'});
                return res.status(200).send({
                    page,
                    total,
                    pages: Math.ceil(total / itemsPerPage),
                    products,
                })
            });
        }else{
            return res.status(404).send({message:'No existe la subcategoría'});
        }
    });



}

function getComments(req,res){
    // usuario del token req.user.sub

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    Product.find({slug:req.params.slug}).exec((err, product)=>{
        if(err) return res.status(500).send({message:'Error en la petición'});
        if(!product) return res.status(404).send('No existe este producto');
        if(product.length > 0){
            Comments.find({product:product[0]._id}).populate('product user','-password -role -email -__v').sort('-created_at').paginate(page,itemsPerPage,(err,comments,total)=>{
                if(err) return res.status(500).send({message:'Error en la petición'});
                if(!comments) return res.status(404).send({message:'No hay comentarios'});

                return res.status(200).send({
                    page,
                    total,
                    pages:Math.ceil(total/itemsPerPage),
                    comments,
                })
            });
        }else{
            return res.status(404).send({message:'No hay comentarios'});
        }
    })

}

// Subir archivos de imagen
function uploadImage(req,res){
    var productId = req.params.id;


    if(req.files){

        var file_path = req.files.image.path;

        var file_split = file_path.split('\\');

        var file_name = file_split[2];

        var ext_split = file_name.split('\.');

        var file_ext = ext_split[1];

        if(req.user.role != 'ROLE_ADMIN'){
            removeFileUploads(res, file_path);
            return res.status(403).send({message:'No tienes permiso para actualizar el producto'});
        }

        if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif'){
            // Actualizar bd

            Product.findByIdAndUpdate(productId, {image: file_name}, {new:true}, (err,productUpdated) => {
                if(err) return res.status(500).send({message: 'Error en la petición'});

                if(!productUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

                return res.status(200).send({
                    product: productUpdated,
                });
            });
        }else{
            removeFileUploads(res, file_path);
            return res.status(403).send({message:'Extension no valida'});
        }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }

}

function removeFileUploads(res, file_path){
    fs.unlink(file_path, (err) => {
        // console.log(err);
    });
}


function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/products/'+image_file;

    console.log(image_file);

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen'});
        }
    })
}

module.exports = {
    home,
    saveProduct,
    getProduct,
    saveComment,
    getComments,
    saveRating,
    uploadImage,
    getImageFile,
    getImportantProducts,
    getProductsBySubCategory
};