'use strict'



// Primera letra mayuscula para saber que es un modelo
var Family = require('../models/family');
var jwt = require('../services/jwt');

function saveFamily(req,res) {
    var params = req.body;

    if(params.family && params.slug){

        var family = new Family();
        family.family = params.family;
        family.slug = params.slug.toLowerCase();
        family.activated = (params.activated) ? params.activated : false;


        Family.countDocuments({family:params.family}).exec((err,countFamily)=>{
            if(countFamily > 0) return res.status(404).send({message:'Ya existe esa familia'});
            if(err) return res.status(500).send({message:'Ha ocurrido un error'});
            family.save((err,familyStored)=>{
                if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el producto'});

                if(familyStored){
                    res.status(200).send({product: familyStored});
                }else{
                    res.status(404).send({message:'No se ha registrado el producto'});
                }
            });

        });




    }else {
        res.status(200).send({
            message: 'Rellena todos los datos',
        });
    }


}


function getCategories(req,res){

    Family.find().exec((err,category)=>{
        return res.status(200).send({
            category
        })
    });
}



module.exports = {
    saveFamily,
    getCategories
};