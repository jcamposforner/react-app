'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// cargar rutas

var user_routes = require('./routes/user');
var product_routes = require('./routes/product');
var family_routes = require('./routes/family');
var stripe_routes = require('./routes/stripe');
var subfamily_routes = require('./routes/subfamily');

// middlewares ( metodo que se ejecuta antes del controlador )

// Convertir a JSON el body
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// cors

// configurar cabeceras http ( allow origin podemos cambiarlo a solo nosotros para proteger cietras llamadas)
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');


    next();
});
//rutas

app.use('/api', user_routes);
app.use('/api', product_routes);
app.use('/api', family_routes)
app.use('/api', subfamily_routes);
app.use('/api', stripe_routes);

// exportar conf

module.exports = app;