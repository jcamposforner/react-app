'use strict'

var express = require('express');
var FamilyController = require('../controllers/family');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/products'});

api.get('/categories',FamilyController.getCategories);

api.post('/family',md_auth.ensureAuth, FamilyController.saveFamily);


module.exports = api;