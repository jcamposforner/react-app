'use strict'

var express = require('express');
var StripeController = require('../controllers/stripe');

var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/users'});


api.post('/charge', StripeController.saveStripe);

module.exports = api;