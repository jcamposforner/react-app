'use strict'

var express = require('express');
var SubFamilyController = require('../controllers/subfamily');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/products'});

api.get('/subfamily/:slug',SubFamilyController.getSubCategoriesByCategory);

api.post('/subfamily',md_auth.ensureAuth, SubFamilyController.saveSubFamily);


module.exports = api;