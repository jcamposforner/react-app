'use strict'

var express = require('express');
var ProductController = require('../controllers/products');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/products'});



api.get('/home', ProductController.home);
api.get('/product/:slug', ProductController.getProduct);
api.get('/productos-destacados',ProductController.getImportantProducts);
api.get('/comment/:slug/:page?',ProductController.getComments);
api.get('/get-image-product/:imageFile', md_auth.ensureAuth, ProductController.getImageFile);
api.get('/products/:slug/:page?',ProductController.getProductsBySubCategory);


api.post('/comment/:slug',md_auth.ensureAuth, ProductController.saveComment);
api.post('/product/add',md_auth.ensureAuth, ProductController.saveProduct);
api.post('/upload-image-product/:id',[md_auth.ensureAuth, md_upload], ProductController.uploadImage);


api.put('/update-rating/:id',md_auth.ensureAuth, ProductController.saveRating);


module.exports = api;