'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentsSchema = Schema({
    body: String,
    created_at: String,
    user: { type: Schema.ObjectId, ref: 'User' },
    product: { type: Schema.ObjectId, ref: 'Product' }
});

module.exports = mongoose.model('Comments', CommentsSchema);