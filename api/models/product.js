'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = Schema({
    title: String,
    description: String,
    price: Number,
    image: String,
    destacado: Boolean,
    totalRating: Number,
    slug: String,
    votes: Number,
    shortdescription: String,
    subfamily: { type: Schema.ObjectId, ref: 'SubFamily' },
    comments: { type: Schema.ObjectId, ref: 'Comments' },
    votesUser: [{ type: Schema.ObjectId, ref: 'User' }],
});

module.exports = mongoose.model('Product', ProductSchema);