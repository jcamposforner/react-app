'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubFamilySchema = Schema({
    subfamily: String,
    family: { type: Schema.ObjectId, ref: 'Family' },
    slug: String,
    activated: Boolean,
});

module.exports = mongoose.model('SubFamily', SubFamilySchema);