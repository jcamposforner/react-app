'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FamilySchema = Schema({
    family: String,
    activated: Boolean,
    slug: String,
});

module.exports = mongoose.model('Family', FamilySchema);